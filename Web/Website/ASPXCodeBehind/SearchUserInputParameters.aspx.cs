﻿//ZD 100147 start
/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Collections.Generic;

namespace ns_SearchUserInputParameters
{
    public partial class SearchUserInputParameters : System.Web.UI.Page
    {
        #region protected data members
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label SelectedUserlabel;
        protected System.Web.UI.WebControls.DataGrid dgConferenceList;
        protected System.Web.UI.WebControls.DataGrid dgUserRooms;
        protected System.Web.UI.WebControls.DataGrid dgUserMCUs;
        protected System.Web.UI.WebControls.DataGrid dgInventoryList;
        protected System.Web.UI.WebControls.DataGrid dgWorkOrder;
        protected System.Web.UI.WebControls.Table tblNoRecords;
        protected System.Web.UI.WebControls.Table tblNoUserRooms;
        protected System.Web.UI.HtmlControls.HtmlButton btnCancel;
        protected System.Web.UI.WebControls.Button btnDelete;
        protected System.Web.UI.WebControls.Button btnReassign;
        protected System.Web.UI.WebControls.Label lblNoItems;
        protected System.Web.UI.HtmlControls.HtmlTableRow trInventory;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMCU;
        protected System.Web.UI.HtmlControls.HtmlTableRow trRooms;
        protected System.Web.UI.HtmlControls.HtmlTableRow trConference;
        protected System.Web.UI.HtmlControls.HtmlTableRow trWorkOrder;
        protected System.Web.UI.HtmlControls.HtmlTable tblUsrSelection;
        protected System.Web.UI.HtmlControls.HtmlTable tblSelecterUsr;
        protected System.Web.UI.HtmlControls.HtmlTable tblFilter;
        protected System.Web.UI.WebControls.TextBox hdnApprover1;
        protected System.Web.UI.WebControls.Table tblPage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSortingOrder;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtSortBy;


        private myVRMNet.NETFunctions obj;
        private ns_Logger.Logger log;
        protected bool isExpressUser = false;
        protected int usrID = 11;
        protected DataSet ds = null;
        XmlWriter _xWriter = null;
        XmlWriterSettings _xSettings = null;
        protected string format = "MM/dd/yyyy";
        string tformat = "hh:mm tt";
        int selectedoption = 0;
        static int totalPages;
        static int pageNo;
        static int SortingOrder = 0;
        static int SortBy = 1;//ZD 101525


        #endregion

        #region Constructor
        public SearchUserInputParameters()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
        }

        #endregion

        #region UserFilterType

        private class UserFilterType
        {
            public const int Conferences = 1;
            public const int Rooms = 2;
            public const int MCUs = 3;
            public const int AV = 4;
            public const int Menu = 5;
            public const int Facilities = 6;
            public const int AVWO = 7;
            public const int MenuWO = 8;
            public const int FacilitiesWO = 9;
        }
        #endregion

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();

            obj.AccessandURLConformityCheck("SearchUserInputParameters.aspx", Request.Url.AbsoluteUri.ToLower());

            if (Session["userID"] != null)
            {
                if (Session["userID"].ToString() != "")
                    int.TryParse(Session["userID"].ToString(), out usrID);
            }

            if (Session["isExpressUser"] != null && Session["isExpressUserAdv"] != null && Session["isExpressManage"] != null)
            {
                if (Session["isExpressUser"].ToString() == "1" || Session["isExpressUserAdv"].ToString() == "1" || Session["isExpressManage"].ToString() == "1")
                    isExpressUser = true;
            }

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }

            if (Session["timeFormat"].ToString().Equals("0"))
                tformat = "HH:mm";
            else if (Session["timeFormat"].ToString().Equals("1"))
                tformat = "hh:mm tt";
            else if (Session["timeFormat"].ToString().Equals("2"))
                tformat = "HHmmZ";

            if (Session["usearch"] != null)
                int.TryParse(Session["usearch"].ToString(), out selectedoption);

            if (!IsPostBack)
                BindData();
        }
        #endregion

        #region BindData

        private void BindData()
        {
            StringBuilder _InXML = new StringBuilder();
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                string pageNoS = "1";
                if (Request.QueryString["pageNo"] != null)
                    pageNoS = Request.QueryString["pageNo"].ToString().Trim();

                _xSettings = new XmlWriterSettings();
                _xSettings.OmitXmlDeclaration = true;
                using (_xWriter = XmlWriter.Create(_InXML, _xSettings))
                {
                    _xWriter.WriteStartElement("UserSearch");
                    _xWriter.WriteElementString("UserID", Session["userID"].ToString());
                    _xWriter.WriteString(obj.OrgXMLElement());

                    if (Session["uid"] != null)
                        _xWriter.WriteElementString("SearchUserID", Session["uid"].ToString());
                    else
                        _xWriter.WriteElementString("SearchUserID", string.Empty);

                    if (Session["usearch"] != null)
                        _xWriter.WriteElementString("SelectedOption", Session["usearch"].ToString());
                    else
                        _xWriter.WriteElementString("SelectedOption", string.Empty);

                    if (pageNoS != null) //ZD 101525
                        _xWriter.WriteElementString("PageNo", pageNoS);
                    else
                        _xWriter.WriteElementString("PageNo", string.Empty);

                    if (txtSortBy.Value.ToString() != null) //ZD 101525
                        _xWriter.WriteElementString("SortBy", txtSortBy.Value.ToString());
                    else
                        _xWriter.WriteElementString("SortBy", string.Empty);

                    if (SortingOrder.ToString() != null) //ZD 101525
                        _xWriter.WriteElementString("SortingOrder", hdnSortingOrder.Value.ToString());
                    else
                        _xWriter.WriteElementString("SortingOrder", string.Empty);


                    _xWriter.WriteFullEndElement();
                    _xWriter.Flush();
                }
                _InXML = _InXML.Replace("&lt;", "<")
                                      .Replace("&gt;", ">");

                string outXML = obj.CallMyVRMServer("SearchUserInfo", _InXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return;
                }
                else
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = null;
                    if (xmldoc.SelectSingleNode("//UserSearch/UserName") != null)
                        SelectedUserlabel.Text = xmldoc.SelectSingleNode("//UserSearch/UserName").InnerText;
                    trConference.Visible = false;
                    trMCU.Visible = false;
                    trRooms.Visible = false;
                    trInventory.Visible = false;
                    trWorkOrder.Visible = false;
                    btnCancel.Visible = true;
                    btnReassign.Visible = true;
                    tblNoRecords.Visible = false;
                    btnDelete.Visible = false;

                    switch (selectedoption)
                    {
                        case UserFilterType.Conferences:
                            tblUsrSelection.Width = "60%";
                            tblSelecterUsr.Width = "60%";
                            nodes = xmldoc.SelectNodes("//UserSearch/AssignedConferences/Conference");
                            if (nodes.Count > 0)
                            {
                                btnDelete.Visible = true;
                                trConference.Visible = true;
                            }
                            break;
                        case UserFilterType.Rooms:
                            tblUsrSelection.Width = "60%";
                            tblSelecterUsr.Width = "60%";
                            nodes = xmldoc.SelectNodes("//UserSearch/AssignedRooms/Room");
                            if (nodes.Count > 0)
                                trRooms.Visible = true;
                            break;
                        case UserFilterType.MCUs:
                            tblUsrSelection.Width = "60%";
                            tblSelecterUsr.Width = "60%";
                            nodes = xmldoc.SelectNodes("//UserSearch/AssignedMCUs/MCU");
                            if (nodes.Count > 0)
                                trMCU.Visible = true;
                            break;
                        case UserFilterType.AV:
                        case UserFilterType.Menu:
                        case UserFilterType.Facilities:
                            //tblUsrSelection.Width = "60%";
                            //tblSelecterUsr.Width = "60%";
                            tblFilter.Width = "60%";
                            nodes = xmldoc.SelectNodes("//UserSearch/AssignedInventories/Inventory");
                            if (nodes.Count > 0)
                                trInventory.Visible = true;
                            break;
                        case UserFilterType.AVWO:
                        case UserFilterType.MenuWO:
                        case UserFilterType.FacilitiesWO:
                            //tblUsrSelection.Width = "70%";
                            //tblSelecterUsr.Width = "70%";
                            tblFilter.Width = "70%";
                            nodes = xmldoc.SelectNodes("//UserSearch/AssignedWorkOrder/WorkOrder");
                            if (nodes.Count > 0)
                                trWorkOrder.Visible = true;
                            break;
                    }

                    if (nodes.Count > 0)
                    {
                        btnCancel.Visible = true;
                        btnReassign.Visible = true;
                        tblNoRecords.Visible = false;

                        //ZD 101525
                        totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//UserSearch/TotalPages").InnerText);
                        pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//UserSearch/PageNo").InnerText);
                        SortBy = Convert.ToInt32(xmldoc.SelectSingleNode("//UserSearch/SortBy").InnerText);

                        if (totalPages > 1)
                            obj.DisplayPaging(totalPages, pageNo, tblPage, "searchuserinputparameters.aspx?");

                        LoadGrid(nodes, selectedoption);
                    }
                    else
                    {
                        tblNoRecords.Visible = true;
                        btnReassign.Visible = false;
                        btnDelete.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            {
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
            }
        }

        #endregion

        #region LoadGrid From Xml

        protected void LoadGrid(XmlNodeList nodes, int selectedoption)
        {
            try
            {
                XmlTextReader xtr;
                ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();
                string confTYpe = "2";
                DateTime ConfDate = DateTime.Now;
                //DateTime ConfTime = DateTime.Now;

                string confDt = "";

                switch (selectedoption)
                {
                    case UserFilterType.Conferences:
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                confTYpe = dr["ConferenceType"].ToString();

                                if (dr["ConferenceType"].ToString() == "2" && dr["isOBTP"].ToString() == "1")//ZD 101525
                                    confTYpe = "9";

                                switch (confTYpe)
                                {
                                    case ns_MyVRMNet.vrmConfType.AudioOnly:
                                        dr["ConferenceType"] = obj.GetTranslatedText("Audio-Only");
                                        break;
                                    case ns_MyVRMNet.vrmConfType.AudioVideo:
                                        dr["ConferenceType"] = obj.GetTranslatedText("Audio/Video");
                                        break;
                                    case ns_MyVRMNet.vrmConfType.P2P:
                                        dr["ConferenceType"] = obj.GetTranslatedText("Point to Point");
                                        break;
                                    case ns_MyVRMNet.vrmConfType.RoomOnly:
                                        dr["ConferenceType"] = obj.GetTranslatedText("Room Only");
                                        break;
                                    case ns_MyVRMNet.vrmConfType.HotDesking:
                                        dr["ConferenceType"] = obj.GetTranslatedText("Hotdesking");
                                        break;
                                    case ns_MyVRMNet.vrmConfType.OBTP:
                                        dr["ConferenceType"] = obj.GetTranslatedText("OBTP");
                                        break;
                                    default:
                                        dr["ConferenceType"] = obj.GetTranslatedText("Undefined");
                                        break;
                                }

                                confDt = dr["ConferenceDateTime"].ToString();

                                DateTime.TryParse(confDt, out ConfDate);

                                dr["ConferenceDateTime"] = ConfDate.ToString(format) + " " + ConfDate.ToString(tformat);
                                
                            }

                            DataSet DS = new DataSet();

                            DS = ds.Copy();

                            DataRow[] drArr = DS.Tables[0].Select("IsRecurInstance = 1");
                            foreach (DataRow dr in drArr)
                            {
                                DS.Tables[0].Rows.Remove(dr);
                            }

                            dgConferenceList.DataSource = DS;
                            dgConferenceList.DataBind();
                        }
                        break;
                    case UserFilterType.Rooms:
                        btnDelete.Visible = false;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            dr["Role"] = obj.GetTranslatedText(dr["Role"].ToString().Trim());
                        }
                        dgUserRooms.DataSource = ds;
                        dgUserRooms.DataBind();
                        break;
                    case UserFilterType.MCUs:
                        btnDelete.Visible = false;
                        //ZD 101714
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            dr["Status"] = obj.GetTranslatedText(dr["Status"].ToString().Trim());
                            dr["Role"] = obj.GetTranslatedText(dr["Role"].ToString().Trim());
                        }

                        dgUserMCUs.DataSource = ds;
                        dgUserMCUs.DataBind();
                        break;
                    case UserFilterType.AV:
                    case UserFilterType.Facilities:
                    case UserFilterType.Menu:
                        btnDelete.Visible = false;
                        dgInventoryList.DataSource = ds.Tables[0];
                        dgInventoryList.DataBind();
                        break;
                    case UserFilterType.AVWO:
                    case UserFilterType.FacilitiesWO:
                    case UserFilterType.MenuWO:
                        if (selectedoption == UserFilterType.MenuWO)
                            dgWorkOrder.Columns[1].HeaderText = obj.GetTranslatedText("Delivery By Date/Time");
                        dgWorkOrder.DataSource = ds.Tables[0];
                        dgWorkOrder.DataBind();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region DataGrid Item DataBound Event Handler

        protected void dgInventoryList_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    if (row != null)
                    {
                        CheckBox select = (CheckBox)e.Item.FindControl("ChkSelect");
                        if (select != null)
                            select.Attributes.Add("id", row["Id"].ToString().Trim());

                        if (selectedoption == 7 || selectedoption == 8 || selectedoption == 9)
                        {
                            DateTime endDate = DateTime.Now;
                            if (row["EndDate"].ToString() != "")
                                endDate = Convert.ToDateTime(row["EndDate"].ToString());

                            e.Item.Cells[1].Text = endDate.ToString(format) + " " + endDate.ToString(tformat);
                        }

                        DataTable dtchild = new DataTable();
                        DataRow[] foundRows;
                        if (selectedoption == 1)
                        {
                            DataGrid gv = new DataGrid();
                            gv = (DataGrid)e.Item.FindControl("dgInstanceList");

                            if (gv != null)
                            {
                                dtchild = ds.Tables[0].Clone();
                                HtmlImage imgdiv = (HtmlImage)e.Item.FindControl("imgdiv" + row["Id"].ToString());
                                if (row["IsRecur"].ToString() == "1")
                                {
                                    if (imgdiv != null)
                                        imgdiv.Visible = true;
                                    //Expand the Child grid
                                    //ClientScript.RegisterStartupScript(GetType(), "Expand", "<SCRIPT LANGUAGE='javascript'>expandcollapse('div" + row["Id"].ToString() + "','one');</script>");

                                    //Prepare the query for Child GridView by passing the Id of the parent row
                                    foundRows = ds.Tables[0].Select("IsRecur = 1 and Id='" + row["Id"].ToString() + "'");

                                    for (int r = 0; r < foundRows.Length; r++)
                                        dtchild.ImportRow(foundRows[r]);

                                    gv.DataSource = dtchild;
                                    gv.DataBind();
                                }
                                else
                                {
                                    if (imgdiv != null)
                                        imgdiv.Visible = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GoBack
        /// <summary>
        /// GoBack
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoBack(Object sender, EventArgs e)
        {
            try
            {
                Session["uid"] = "";
                Session["usearch"] = "";
                Response.Redirect("SearchConferenceInputParameters.aspx");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region ReassignUser
        /// <summary>
        /// ReassignUser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReassignUser(object sender, EventArgs e)
        {
            StringBuilder _InXML = new StringBuilder();
            XmlDocument xmlDoc = new XmlDocument();
            CheckBox chkSelect = null;
            bool isUserSelected = false;
            int selectedoption = 0;
            Button btn = sender as Button;

            if (Session["usearch"] != null)
                int.TryParse(Session["usearch"].ToString(), out selectedoption);
            try
            {
                if (btn != null && btn.ID == "btnDelete" && Session["uid"] != null && string.IsNullOrWhiteSpace(hdnApprover1.Text.Trim()))
                {
                    errLabel.Text = obj.GetTranslatedText("A user must be assigned to the deleted conferences.");
                    errLabel.Visible = false;
                    return;
                }

                _xSettings = new XmlWriterSettings();
                _xSettings.OmitXmlDeclaration = true;
                using (_xWriter = XmlWriter.Create(_InXML, _xSettings))
                {
                    _xWriter.WriteStartElement("SetAssignedUserAdmin");
                    _xWriter.WriteElementString("UserID", Session["userID"].ToString());
                    _xWriter.WriteString(obj.OrgXMLElement());

                    if (Session["uid"] != null)
                        _xWriter.WriteElementString("ExistingUserID", Session["uid"].ToString());
                    else
                        _xWriter.WriteElementString("ExistingUserID", string.Empty);

                    if (Session["usearch"] != null)
                        _xWriter.WriteElementString("SelectedOption", Session["usearch"].ToString());
                    else
                        _xWriter.WriteElementString("SelectedOption", string.Empty);

                    if (Session["uid"] != null)
                        _xWriter.WriteElementString("NewUserID", hdnApprover1.Text);
                    else
                        _xWriter.WriteElementString("NewUserID", string.Empty);

                    if (btn != null && btn.ID == "btnDelete")
                        _xWriter.WriteElementString("IsDeleted", "1");
                    else
                        _xWriter.WriteElementString("IsDeleted", string.Empty);

                    _xWriter.WriteStartElement("SelectedID");
                    switch (selectedoption)
                    {
                        case UserFilterType.Conferences:
                            for (int i = 0; i < dgConferenceList.Items.Count; i++)
                            {
                                chkSelect = (CheckBox)dgConferenceList.Items[i].FindControl("ChkSelect");
                                if (chkSelect != null)
                                {
                                    if (chkSelect.Checked)
                                    {
                                        _xWriter.WriteElementString("ID", chkSelect.Attributes["id"]);
                                        isUserSelected = true;
                                    }
                                }
                            }
                            break;
                        case UserFilterType.Rooms:

                            for (int i = 0; i < dgUserRooms.Items.Count; i++)
                            {
                                chkSelect = (CheckBox)dgUserRooms.Items[i].FindControl("ChkSelect");
                                if (chkSelect != null)
                                {
                                    if (chkSelect.Checked)
                                    {
                                        _xWriter.WriteElementString("ID", chkSelect.Attributes["id"]);
                                        isUserSelected = true;
                                    }
                                }
                            }
                            break;
                        case UserFilterType.MCUs:
                            for (int i = 0; i < dgUserMCUs.Items.Count; i++)
                            {
                                chkSelect = (CheckBox)dgUserMCUs.Items[i].FindControl("ChkSelect");
                                if (chkSelect != null)
                                {
                                    if (chkSelect.Checked)
                                    {
                                        isUserSelected = true;
                                        _xWriter.WriteElementString("ID", chkSelect.Attributes["id"]);
                                    }
                                }
                            }
                            break;
                        case UserFilterType.AV:
                        case UserFilterType.Menu:
                        case UserFilterType.Facilities:
                            for (int i = 0; i < dgInventoryList.Items.Count; i++)
                            {
                                chkSelect = (CheckBox)dgInventoryList.Items[i].FindControl("ChkSelect");
                                if (chkSelect != null)
                                {
                                    if (chkSelect.Checked)
                                    {
                                        isUserSelected = true;
                                        _xWriter.WriteElementString("ID", chkSelect.Attributes["id"]);
                                    }
                                }
                            }
                            break;
                        case UserFilterType.AVWO:
                        case UserFilterType.MenuWO:
                        case UserFilterType.FacilitiesWO:
                            for (int i = 0; i < dgWorkOrder.Items.Count; i++)
                            {
                                chkSelect = (CheckBox)dgWorkOrder.Items[i].FindControl("ChkSelect");
                                if (chkSelect != null)
                                {
                                    if (chkSelect.Checked)
                                    {
                                        isUserSelected = true;
                                        _xWriter.WriteElementString("ID", chkSelect.Attributes["id"]);
                                    }
                                }
                            }
                            break;
                    }

                    _xWriter.WriteFullEndElement();
                    _xWriter.WriteFullEndElement();
                    _xWriter.Flush();
                }
                if (!isUserSelected && (hdnApprover1 != null && hdnApprover1.Text != ""))
                {
                    errLabel.Text = obj.GetTranslatedText("Please select a record for reassigning.");
                    errLabel.Visible = true;
                    return;
                }
                _InXML = _InXML.Replace("&lt;", "<")
                                      .Replace("&gt;", ">");

                string outXML = obj.CallMyVRMServer("SetAssignedUserAdmin", _InXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    BindData();
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return;
                }
                else
                {
                    errLabel.Text = obj.ShowSuccessMessage();
                    errLabel.Visible = true;
                    BindData();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion
        //ZD 101525
        protected void SortGrid(Object sender, CommandEventArgs e)
        {
            try
            {
                txtSortBy.Value = e.CommandArgument.ToString();
                BindData();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SortGrid:" + ex.Message);
            }
        }



    }
}