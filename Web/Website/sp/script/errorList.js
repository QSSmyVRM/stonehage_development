/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100866 End*/
// max 220

//user info
var EN_1 = "Por favor, introduzca un nombre de usuario de inicio de sesi\u00f3n.";
var EN_2 = "Por favor, introduzca la contrase\u00f1a. ";
var EN_3 = "Sus entradas de contrase\u00f1a no coinciden.";
var EN_4 = "Por favor, introduzca un Nombre.";
var EN_5 = "Por favor, introduzca un Apellido.";
var EN_6 = "Por favor, escriba una direcci\u00f3n de correo electr\u00f3nico v\u00e1lida.";
var EN_51 = "Por favor, seleccione un Usuario de la Lista de Usuarios";
var EN_81 = "Por favor, introduzca un correo-e de empresa v\u00e1lido";
var EN_89 = "Por favor, introduzca su nombre completo"
var EN_90 = "Por favor, introduzca un Asunto";
var EN_91 = "Por favor, introduzca un Comentario."
var EN_129 = "Por favor, introduzca un nombre de contacto.";
var EN_130 = "Por Favor, introduzca el n\u00e9mero de tel\u00e9fono de contacto.";
var EN_172 = "Por favor, introduzca el correo-e del inicio de sesi\u00f3n de usuario.";
var EN_200 = "Las dos direcciones de correo-e no coinciden.";


//date and time info
var EN_7 = "Por favor, compruebe la fecha DESDE y escr\u00edbala de nuevo.";
var EN_8 = "La fecha DESDE no puede ser una fecha futura.";
var EN_9 = "Por favor, escriba una fecha DESDE anterior a la fecha HASTA.";
var EN_17 = "Por favor,  escriba un n\u00e9mero en el campo \u0027Tiempo Antes\u0027.";
var EN_18 = "Por favor,  escriba un n\u00e9mero en el campo \u0027Tiempo Delta\u0027.";
var EN_69 = "Por favor, introduzca la fecha HASTA.";
var EN_70 = "Por favor, introduzca la fecha DESDE.";
var EN_92 = "La fecha HASTA no puede ser una fecha futura.  ";
var EN_93 = "Por favor, escriba una fecha DESDE anterior a la fecha HASTA."
var EN_117 = "Por favor, introduzca la fecha Antes de.";
var EN_118 = "La fecha DESDE no puede ser una fecha pasada.";
var EN_119 = "La fecha 'Antes de' no puede ser una fecha pasada.";
var EN_120 = "Esta fecha ya fue seleccionada.  Por favor, seleccione otra.";
var EN_177 = "Por favor, introduzca una Hora de finalizaci\u00f3n al menos 15 minutos posterior a la Hora de inicio.";
var EN_202 = "El cambio de fecha para personalizaci\u00f3n del caso no est\u00e1 permitido. Por favor, arrastre el caso a otra franja horaria. La hora no cambiar\u00e1.";
var EN_215 = "Por favor, introduzca la hora en el formato correcto (hh:mm AM/PM)";
var EN_216 = "Por favor, compruebe la hora de inicio del sal\u00f3n con la hora final de la conferencia";
var EN_217 = "Por favor, compruebe la hora final del sal\u00f3n con la hora final de la conferencia";
var EN_218 = "Por favor, compruebe la hora de inicio del sal\u00f3n con la hora de inicio de la conferencia.";
var EN_220 = "Por favor, compruebe la hora final del sal\u00f3n con la hora de inicio de la conferencia";

// bridge and port info
var EN_11 = "Por favor, introduzca un nombre MCU.";
var EN_12 = "Por favor, introduzca una direcci\u00f3n de MCU.";
var EN_13 = "Por favor, introduzca un inicio de sesi\u00f3n de MCU.";
var EN_19 = "Por favor,  introduzca un n\u00e9mero en el Intervalo de la encuesta.";
var EN_20 = "Por favor, introduzca un n\u00e9mero (entre 1 y 12) en \u0027Canales T1 por tarjeta\u0027.";
var EN_21 = "Por favor, introduzca un n\u00e9mero (entre 1 y 12) en \u0027N\u00e9mero de puertos IP por tarjeta\u0027.";
var EN_22 = "Por favor, introduzca un n\u00e9mero (entre 1 y 12) en \u0027N\u00e9mero de canales T1 reservados por tarjeta\u0027.";
var EN_23 = "Por favor, introduzca un n\u00e9mero (entre 1 y 12) en \u0027N\u00e9mero de puertos IP reservados por tarjeta\u0027..";
var EN_24 = "Por favor, introduzca un n\u00e9mero (entre 1 y 12) en \u0027N\u00e9mero de puertos de audio por tarjeta\u0027..";
var EN_25 = "Por favor, introduzca un n\u00e9mero (entre 1 y 12) en \u0027N\u00e9mero de puertos IP reservados\u0027.";
var EN_28 = "Por favor, introduzca una direcci\u00f3n de IP v\u00e1lida.";
var EN_82 = "Por favor, introduzca una IP o Nombre SMTP remoto.";
var EN_83 = "Por favor, introduzca un n\u00e9m. de puerto.";
var EN_94 = "Por favor, introduzca un N\u00e9mero de tel\u00e9fono ISDN";
var EN_95 = "Por favor, compruebe el n\u00e9mero de tel\u00e9fono ISDN y escr\u00edbalo de nuevo.";
var EN_96 = "Por favor, compruebe la direcci\u00f3n IP y escr\u00edbala de nuevo.";
var EN_98 = "Por favor, introduzca un n\u00e9mero ISDN v\u00e1lido.";
var EN_122 = "Por favor, introduzca un nombre de servicio.";
var EN_123 = "Por favor, introduzca una direcci\u00f3n.";
var EN_124 = "Por favor, introduzca un Rango Inicial v\u00e1lido.";
var EN_125 = "Por favor, introduzca un Rango Final v\u00e1lido.";
var EN_126 = "Por favor, introduzca un Rango final mayor que el Rango inicial.";
var EN_133 = "Por favor, introduzca un Nombre Cascada.";
var EN_134 = "Por favor, seleccione un Tipo de Protocolo.";
var EN_135 = "Por favor, seleccione un Tipo de Conexi\u00f3n.";
var EN_136 = "Por favor, seleccione un Puente.";
var EN_137 = "Por favor, introduzca una direcci\u00f3n IP o ISDN v\u00e1lidas para el puente.";
var EN_138 = "Por favor, seleccione un Servicio IP por defecto.";
var EN_139 = "Por favor, seleccione un Servicio ISDN por defecto.";
var EN_141 = "Por favor, seleccione un servicio privado.";
var EN_142 = "Por favor, seleccione un servicio p\u00e9blico.";
var EN_143 = "Por favor, seleccione un Medio.";
var EN_171 = "Por favor, seleccione un tipo de interfaz.";
var EN_181 = "Por favor, introduzca un Prefijo. ";
var EN_182 = "Por favor, introduzca un nombre de Punto final.";
var EN_185 = "Por favor, introduzca una direcci\u00f3n de Punto final.";
var EN_189 = "Por favor, rellene con un n\u00e9mero entre 0 y 100 en el campo \u0027% de puerto reservado\u0027.";
var EN_213 = "Por favor, introduzca una direcci\u00f3n IP v\u00e1lida para el Puerto de Control.";
var EN_214 = "Por favor, introduzca una direcci\u00f3n IP v\u00e1lida para el Puerto A.";
var EN_314 = "Por favor, introduzca una Duraci\u00f3n v\u00e1lida.";
var EN_315 = "La duraci\u00f3n de la conferencia debe ser de al menos 15 min. (Diferencia entre la duraci\u00f3n y el periodo regulador)";
var EN_316 = "Por favor, introduzca una Duraci\u00f3n de Inicio de Conferencia v\u00e1lida.";
var EN_317 = "Por favor, introduzca una Duraci\u00f3n de finalizaci\u00f3n de Conferencia v\u00e1lida.";

//Group info
var EN_10 = "Por favor, introduzca un nombre de Grupo.";
var EN_39 = "Por favor, seleccione grupos diferentes para Grupos disponibles y Grupos CC"
var EN_53 = "Por favor, haga \u0027doble-clic\u0027 sobre el Nombre del Grupo.";
var EN_99 = "El Grupo debe tener al menos un miembro.";
var EN_105 = "Por favor, seleccione grupos diferentes para Grupo por defecto y Grupo CC por defecto.";
var EN_128 = "No hay participantes disponibles para ser a\u00f1adido a un grupo. Por favor, seleccione al menos un participante de la libreta de direcciones en primer lugar para a\u00f1adirlos a un grupo."; //FB 1914

//recurring
var EN_32 = "Por favor, compruebe la Hora Inicial Recurrente y vu\u00e9lvala a escribir.";
var EN_33 = "Por favor, compruebe la Duraci\u00f3n Recurrente y vu\u00e9lvala a escribir.";
var EN_34 = "Por favor, compruebe la Zona horaria Recurrente y vu\u00e9lvala a escribir.";
var EN_35 = "Por favor, compruebe el Tipo de Patr\u00f3n Recurrente y vu\u00e9lvalo a escribir.";
var EN_36 = "Por favor, compruebe el Tipo final del Rango de recurrencia y vu\u00e9lvalo a escribir.";
var EN_37 = "Por favor, compruebe el Patr\u00f3n del Rango Recurrente y vu\u00e9lvalo a escribir.";
var EN_38 = "Por favor, compruebe el Tipo de Rango Recurrente y vu\u00e9lvalo a escribir.";
var EN_74 = "Por favor, introduzca una Fecha inicial recurrente futura.\n(formato de fecha: mm/dd/yyyy)";
var EN_79 = "El n\u00e9mero de participantes para una Conferencia Recurrente no puede ser mayor de ";
var EN_107 = "Por favor, seleccione un d\u00eda de la semana.";
var EN_108 = "Por favor, introduzca una \u0027Fecha Final antes de\u0027 futura.\n(formato de fecha: mm/dd/yyyy)";
var EN_109 = "Por favor, introduzca una \u0027Fecha Final\u0027 futura.\n(formato de fecha: mm/dd/yyyy)";
var EN_193 = "Por favor, seleccione al menos una fecha.";
var EN_211 = "Ha alcanzado el l\u00edmite m\u00e1ximo de fechas seleccionadas por el cliente.";

//conference and conference room
var EN_26 = "Por favor, introduzca un nombre del sal\u00f3n de conferencia.";
var EN_27 = "Por favor,  escriba un n\u00e9mero en el campo \u0027Capacidad\u0027.";
var EN_30 = "Por favor, introduzca un nombre de conferencia.";
//var EN_31 = "Please select a Conference Duration of at least 15 minutes.";
var EN_31 = "La conferencia tiene que ser de al menos 15 minutos.";
var EN_40 = "Por favor, introduzca un n\u00e9mero en \u0027N\u00e9mero de Participantes adicionales\u0027.";
var EN_41 = "Por favor, introduzca un n\u00e9mero positivo en \u0027N\u00e9mero de Participantes adicionales\u0027.";
var EN_43 = "Por favor, seleccione un sal\u00f3n de conferencia.";
var EN_44 = "Por favor, introduzca una contrase\u00f1a de conferencia.";
var EN_45 = "Por favor, compruebe el formato de la fecha de la conferencia y escr\u00edbalo de nuevo.";
var EN_49 = "Por favor, introduzca una fecha de conferencia futura.";
var EN_52 = "Por favor, introduzca toda la informaci\u00f3n  del Nuevo Usuario\nantes de remplazar el usuario previo.";
var EN_54 = "Por favor, haga \u0027doble-clic\u0027 sobre el Nombre del sal\u00f3n.";
var EN_61 = "El(Los) sal\u00f3n(es) siguiente(s) no tiene(n) equipo de v�deo.  \u00bfContin\u00e9a estableciendo la videoconferencia? ";
var EN_62 = "Durante este periodo, la(s) siguiente(s) conferencia(s) est\u00e1n tambi\u00e9n planificadas"
var EN_71 = "Por favor, seleccione el sal\u00f3n para los Asistentes Externos.";
var EN_72 = "Por favor, selecciones Asistentes Externos antes de seleccionar cualquier sal\u00f3n.";
var EN_75 = "Por favor, seleccione el sal\u00f3n para los Asistentes Externos haciendo \u0027clic\u0027 en \u0027Salones Programados\u0027.";
var EN_76 = "Debe seleccionar Asistentes Externos y salones \nantes de programar una conferencia en un sal\u00f3n.";
var EN_87 = "Por favor, seleccione una de las opciones anteriores.";
var EN_97 = "Por favor, introduzca un n\u00e9mero de sal\u00f3n de conferencia.";
var EN_102 = "Por favor, introduzca la fecha de la conferencia.";
var EN_103 = "Por favor, compruebe el N\u00e9mero de Participantes adicionales y vu\u00e9lvalo a escribir.";
var EN_104 = "Por favor, seleccione el sal\u00f3n para los Asistentes Externos.";
var EN_112 = "Por favor, seleccione su sal\u00f3n antes de enviar la solicitud.";
var EN_113 = "Por favor, seleccione una Conferencia.";
var EN_114 = "Por favor, seleccione un sal\u00f3n de la lista de Salones Seleccionados.";
var EN_131 = "Por favor, introduzca un n\u00e9mero en el campo \u0027N\u00e9mero m\u00e1ximo de llamadas de tel\u00e9fono concurrentes'.";
var EN_140 = "Se ha alcanzado el n\u00e9mero m\u00e1ximo de 512 caracteres permitidos para la Descripci\u00f3n de la Conferencia.";
var EN_144 = "Usted ha elegido uno o m\u00e1s salones sin participantes. \u00bfDesea continuar?"
var EN_145 = "Por favor, seleccione al menos un Protocolo.";
var EN_146 = "Por favor, seleccione al menos un Protocolo de Audio.";
var EN_147 = "Por favor, seleccione al menos un Protocolo de V\u00eddeo.";
var EN_148 = "Por favor, seleccione al menos una Velocidad de l\u00ednea.";
var EN_149 = "Por favor, seleccione el Equipo por defecto";
var EN_150 = "Por favor, introduzca el n\u00e9mero de tel\u00e9fono del sal\u00f3n.";
var EN_186 = "No hay participantes seleccionados en el sal\u00f3n. \u00bfEst\u00e1 seguro de que desea continuar?";
var EN_187 = "La contrase\u00f1a de la conferencia deber\u00eda ser solo num\u00e9rica (0-99999).";
var EN_190 = "Por favor, seleccione un punto final para ver los detalles.";
var EN_192 = "Por favor, introduzca hasta 10 correos-e para M\u00e9ltiples correos-e de asistente.";
var EN_194 = "Por favor seleccione Auxiliar a Cargo de la direcci�n book.\n To libro espect�culo de direcciones, haga clic en editar icono a la derecha del campo de texto.";
var EN_195 = "Por favor, seleccione un sal\u00f3n de la lista o proporcione su propia informaci\u00f3n de conexi\u00f3n";
var EN_203 = "Por favor, seleccione al menos un departamento para asociar con el sal\u00f3n.";
var EN_204 = "Cuando seleccione el punto final, por favor seleccione el Medio para \u00e9l.";
var EN_205 = "Por favor, seleccione un punto final, porque selecciona un tipo de medio.";
var EN_208 = "Ha alcanzado el l\u00edmite m\u00e1ximo de caracteres permitidos para este campo.";
var EN_212 = "La asignaci\u00f3n avanzada del sal\u00f3n s\u00f3lo puede ser asignada a una conferencia sin-v\u00eddeo.\n Por favor, o elimine la asignaci\u00f3n avanzada del sal\u00f3n o cree una conferencia sin v\u00eddeo.";
var EN_219 = "El sal\u00f3n ya ha sido a\u00f1adido.";

//Template
var EN_50 = "Por favor, introduzca el Nombre de plantilla.";
var EN_63 = "Se puede seleccionar un m\u00e1ximo de cinco plantillas.";

var EN_55 = "\u00bfEst\u00e1 seguro de que desea quitar este grupo?";
var EN_56 = "\u00bfEst\u00e1 seguro de que desea quitar los grupos seleccionados?";
var EN_57 = "\u00bfEst\u00e1 seguro de que desea quitar esta conferencia? \nTodos los participantes invitados previamente ser\u00e1n notificados de la cancelaci\u00f3n.";
var EN_58 = "\u00bfEst\u00e1 seguro de que desea quitar la(s) conferencia(s) seleccionada(s)?";
var EN_59 = "\u00bfEst\u00e1 seguro de que desea quitar esta plantilla?";
var EN_60 = "\u00bfEst\u00e1 seguro de que desea quitar las plantillas seleccionadas?";
var EN_77 = "Por favor, haga 'doble-clic' sobre la Plantilla o la Conferencia.";
var EN_191 = "Por favor, introduzca un n\u00e9mero entero positivo en la Hora inicial.";

// Food/Resource
var EN_156 = "Por favor, introduzca el Nombre del Pedido.";
var EN_157 = "Un pedido debe contener al menos un alimento. Si elije continuar, este pedido ser\u00e1 borrado. \n\u00bfEst\u00e1 seguro de que desea continuar?";
var EN_158 = "Por favor, seleccione una imagen.";
var EN_159 = "Por favor, seleccione una imagen del recurso.";
var EN_160 = "Por favor, seleccione un art\u00edculo del recurso.";
var EN_161 = "Por favor, introduzca el Nombre del art\u00edculo.";
var EN_162 = "Por favor, introduzca una Cantidad correcta del art\u00edculo. Deber\u00eda ser un n\u00e9mero entero positivo."
var EN_163 = "Si la cantidad del art\u00edculo es 0, este art\u00edculo se eliminar\u00e1.\n\u00bfSeguro de que desea continuar?";
var EN_164 = "Este art\u00edculo del rescurso ya existe en la lista. Por favor, introduzca un nombre diferente o selecci\u00f3nelo para editarlo.";
var EN_165 = "Por favor, introduzca un precio correcto. Deber\u00eda ser un n\u00e9mero positivo y hasta con dos decimales.";
var EN_166 = "Todos los campos est\u00e1n vacios.\nEste pedido no se guardar\u00e1 si es un pedido nuevo;\n o ser\u00e1 eliminado si es un pedido viejo.\n\n\u00bfEst\u00e1 seguro de que desea continuar?";
var EN_167 = "Su pedido para este sal\u00f3n ser\u00e1 guardado.\nAdvertencia: Un pedido debe contener al menos un art\u00edculo del recurso. Si elije continuar, este pedido ser\u00e1 borrado. \n\u00bfEst\u00e1 seguro de que desea continuar?";
var EN_168 = "Por favor, introduzca el Nombre del art\u00edculo, o borre este art\u00edculo nuevo.";
var EN_169 = "Por favor, seleccione una Categor\u00eda.";
var EN_170 = "Por favor, introduzca el Precio de la unidad del art\u00edculo.";
var EN_178 = "El sistema s\u00f3lo soporta im\u00e1genes en formato JPG y JPEG. Por favor, convi\u00e9rtala a formato JPG o JPEG usando alguna herramienta gr\u00e1fica, p.ej. Microsoft Photo Editor.";
var EN_179 = "Por favor, no cargue archivos ejecutables para evitar cualquier virus.";
var EN_188 = "Por favor, introduzca un n\u00e9mero entero positivo en 'Cantidad del art\u00edculo'.";

// terminal control
var EN_183 = "Por favor, introduzca el Nombre del punto final.";
var EN_184 = "Por favor, introduzca un correo-e de Invitado v\u00e1lido.";
var EN_206 = "\u00bfEst\u00e1 seguro de que desea concluir con el punto final seleccionado?";


//misc
var EN_64 = "Debe introducir un Usuario nuevo para reemplazar al Usuario previo.";
var EN_65 = "Conferencia guardada correctamente en su Calendario de IBM Notes."; //102743
var EN_66 = "Conferencia guardada correctamente en su Calendario de Outlook.";
var EN_67 = "Conferencia guardada correctamente en su Calendario de Outlook.";
var EN_68 = "Lo sentimos, esta p\u00e1gina ha caducado. Por favor, haga 'clic' aqu\u00ed para iniciar sesi\u00f3n otra vez.";
var EN_14 = "Por favor, introduzca un n\u00e9mero positivo o negativo en la Posici\u00f3n en Cadena.";
var EN_15 = "El n\u00e9mero de la tarjeta no es un n\u00e9mero positivo ni negativo, \no es mayor que el n\u00e9mero m\u00e1ximo.";
var EN_16 = "Por favor, introduzca un Enlace de la p\u00e1gina de inicio del usuario.";
var EN_73 = "La etiqueta de la URL en la descripci\u00f3n de la conferencia deber\u00eda ir en parejas (<url></url>). \nPor favor, corr\u00edjala y pruebe de nuevo.";
var EN_78 = "Por favor, use el bot\u00f3n SELECCIONAR para seleccionar un Sal\u00f3n de Conferencia Principal";
var EN_80 = "Por favor, haga 'doble-clic' sobre 'Ubicaci\u00f3n'.";
var EN_84 = "Por favor, introduzca un Tiempo de desactivaci\u00f3n de la conexi\u00f3n.";
var EN_85 = "Por favor, compruebe la fecha HASTA y vu\u00e9lvala a escribir.";
var EN_86 = "Sorry, ning\u00e9n Recurso tiene el ID de recurso de ";
var EN_88 = "La Ventana Original est\u00e1 perdida. Esta ventana se cerrar\u00e1.\nPor favor, vuelva a la p\u00e1gina original e int\u00e9ntelo otra vez.";
var EN_100 = "El comentario no puede contener m\u00e1s de 500 caracteres.";
var EN_106 = "Por favor, espere mientras procesamos su solicitud.";
var EN_110 = "Por favor, introduzca la Informaci\u00f3n del Usuario nuevo.";
var EN_111 = "El Usuario nuevo y el Usuario previo tienen el mismo correo-e. Por favor, escr\u00edbalo de nuevo.";
var EN_115 = "Por favor, seleccione un valor en la Sesi\u00f3n de V\u00eddeo.";
var EN_116 = "Su navegador no soporta la impresi\u00f3n. Por favor, use la opci\u00f3n del men\u00e9 para imprimir."
var EN_127 = "Advertencia: Ha cambiado las preferencias de la plantilla.\n\u00bfGuardar estos cambios nuevos?";
var EN_151 = "Por favor, introduzca el Nombre de la Categor\u00eda.";
var EN_152 = "Por favor, introduzca el Nombre del archivo.";
var EN_154 = "Lo sentimos, no puede seleccionar 'Ordenar por sal\u00f3n' ni no ha elegido ning\u00e9n sal\u00f3n.";
var EN_155 = "Lo sentimos, no puede seleccionar 'Ordenar por sal\u00f3n' ni ha elegido 'Ninguno' o 'Cualquiera' en la opci\u00f3n Sal\u00f3n.";
var EN_174 = "\u00bfEst\u00e1 seguro de que desea quitar este archivo?";
var EN_175 = "Error: fallo al cargar el archivo [raz\u00f3n - carpeta]. Por favor, proporcione esta informaci\u00f3n al administrador.";
var EN_176 = "Error: hay algunos archivos duplicados en la lista de archivos de carga.";
var EN_180 = "Por favor, seleccione al menos un informe para eliminar.";
var EN_196 = "Por favor, introduzca una Tarifa del Puerto MCU ISDN correcta. Deber\u00eda ser un n\u00e9mero positivo con hasta dos decimales.";
var EN_197 = "Por favor, introduzca un coste de L\u00ednea ISDN correcto. Deber\u00eda ser un n\u00e9mero positivo con hasta dos decimales.";
var EN_196 = "Por favor, introduzca una Tarifa del Puerto IP correcta. Deber\u00eda ser un n\u00e9mero positivo con hasta dos decimales.";
var EN_199 = "Por favor, introduzca un coste de L\u00ednea IP correcto. Deber\u00eda ser un n\u00e9mero positivo con hasta dos decimales.";
var EN_201 = "Esta p\u00e1gina ha caducado por inactividad.\nPor favor, inicie sesi\u00f3n e int\u00e9ntelo otra vez.";
var EN_207 = "Lo sentimos, ha ocurrido alg\u00e9n error en la SIM. Puede ser causado por Internet o por el Sistema,\Por favor, int\u00e9ntelo otra vez. Si contin\u00e9a ocurriendo, por favor notif\u00edquelo al administrador.\nLa SIM se desconectar\u00e1 ahora."
var EN_209 = "S\u00f3lo se permiten caracteres alfanum\u00e9ricos en los nombres de archivos.";
var EN_210 = "Por favor, introduzca una ID de correo-e \u00e9nica.";

// browser
var EN_132 = "Su navegador tiene habilitado el bloqueo de pop-ups. Por favor, inhabil\u00edtelo para el Sitio Web myVMR, ya que myVMR usa los pop-ups que son necesarios para que usted introduzca la informaci\u00f3n.";


